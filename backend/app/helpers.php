<?php
use Tymon\JWTAuth\Facades\JWTAuth;

@ob_start();
session_start();

function base_url()
{
	# code...
	return env('BASE_URL',url('/') );
}
function storage_url()
{
	# code...

	return url('/').'/storage';
}
function cdn_url()
{
	# code...
	return env('CDN_URL',url('/') );
}
function admin_url()
{
	return url('/');
}

function str_slug_u($value='')
{
	return str_replace(" ","_",$value);
}


 // Get User data by token
function getJwtUser(){
    try {
        $user = JWTAuth::parseToken()->authenticate();
    } catch (Exception $e) {
        return false;
    }
    return $user;
}

?>
