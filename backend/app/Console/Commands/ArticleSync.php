<?php

namespace App\Console\Commands;

use App\Models\Article;
use App\Models\ArticleSource;
use Illuminate\Console\Command;

class ArticleSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:article-sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        try {
            $article_source = ArticleSource::get();

            $curl = curl_init();
            if (isset($article_source[0])) {
                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://newsapi.org/v2/top-headlines?country=in&apiKey=' . $article_source[0]->api_key,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_USERAGENT => 'MyApp/1.0',
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'GET',
                ));

                $response = curl_exec($curl);
                $data = json_decode($response,true);


                // dd($data);
                if($data["status"] == "ok"){
                    foreach ($data["articles"] as $key => $article) {
                        $articleArr = [
                            'title' => $article["title"],
                            'description' => $article["description"]?$article["description"]:'',
                            'source' => $article["source"]["name"],
                            'author' => $article["author"],
                            'source_id' => $article_source[0]->id,
                            'url' => $article["url"],
                            'image' => $article["urlToImage"]?$article["urlToImage"]:'',
                            'published_at' => date('Y-m-d',strtotime($article["publishedAt"]))
                        ];

                        if($existArticle = Article::where('title',$article["title"])->where('source_id',$article_source[0]->id)->first()
                        ){
                            Article::where('id',$existArticle->id)->update($articleArr);
                        }else{
                            Article::create($articleArr);
                        }
                    }
                }

            }
            if (isset($article_source[1])) {
                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://api.nytimes.com/svc/mostpopular/v2/emailed/7.json?api-key=' . $article_source[1]->api_key,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'GET',
                ));

                $response = curl_exec($curl);

                $data = json_decode($response,true);
                if($data["status"] == "OK"){
                    foreach ($data["results"] as $key => $article) {

                        $articleArr = [
                            'title' => $article["title"],
                            'description' => $article["abstract"],
                            'source' => $article["source"],
                            'url' => $article["url"],
                            'source_id' => $article_source[1]->id,
                            'image' => count($article["media"])>0?$article["media"][0]["media-metadata"][2]["url"]:"",
                            'published_at' => date('Y-m-d',strtotime($article["published_date"]))
                        ];

                        if($existArticle = Article::where('title',$article["title"])->where('source_id',$article_source[1]->id)->first()
                        ){
                            Article::where('id',$existArticle->id)->update($articleArr);
                        }else{
                            Article::create($articleArr);
                        }
                    }
                }

            }
            if (isset($article_source[2])) {
                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://newsdata.io/api/1/news?apikey=' . $article_source[2]->api_key . '&language=en',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'GET',
                ));

                $response = curl_exec($curl);

                $data = json_decode($response,true);

                if($data["status"] == "success"){
                    foreach ($data["results"] as $key => $article) {

                        $articleArr = [
                            'title' => $article["title"],
                            'description' => $article["description"]?$article["description"]:'',
                            'source' => $article["source_id"],
                            'url' => $article["link"],
                            'source_id' => $article_source[2]->id,
                            'author' => ($article['creator'] && count($article['creator'])) > 0 ? $article['creator'][0]: null,
                            'image' => $article['image_url']?$article['image_url']:'',
                            'published_at' => date('Y-m-d',strtotime($article["pubDate"]))
                        ];

                        if($existArticle = Article::where('title',$article["title"])->where('source_id',$article_source[2]->id)->first()
                        ){
                            Article::where('id',$existArticle->id)->update($articleArr);
                        }else{
                            Article::create($articleArr);
                        }
                    }
                }
            }

            curl_close($curl);
        } catch (\Exception $th) {
            //throw $th;
            dd($th);
        }

    }
}
