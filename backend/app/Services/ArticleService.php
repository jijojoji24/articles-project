<?php

namespace App\Services;

use App\Repositories\ArticleRepositoryInterface;

class ArticleService
{
    protected $articleRepository;

    public function __construct(ArticleRepositoryInterface $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }
    public function getArticles($user)
    {
        return $this->articleRepository->getArticles($user->id);
    }

    public function getArticleSources($user)
    {
        return $this->articleRepository->getArticleSources($user->id);
    }
}
