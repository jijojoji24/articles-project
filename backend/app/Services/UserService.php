<?php

namespace App\Services;

use App\Repositories\UserRepositoryInterface;

class UserService
{
    protected $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getUsers()
    {
        return $this->userRepository->getUsers();
    }

    public function getUserById($id)
    {
        return $this->userRepository->getUserById($id);
    }

    public function storeUser($payload)
    {
        return $this->userRepository->storeUser($payload);
    }

    public function getPreference($user)
    {
        return $this->userRepository->getPreference($user->id);
    }
    public function getSource($user)
    {
        return $this->userRepository->getSource($user->id);
    }
    public function storeSettings($payload)
    {
        return $this->userRepository->storeSettings($payload);
    }
}
