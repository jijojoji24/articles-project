<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Validator;

class AuthController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }
    // login user authentication
    public function checklogin(Request $request)
    {
        $rules = [
            'email' => 'required',
            'password' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->messages(), 'errorcode' => 1], 200);
        }
        $input = $request->all();
        try {

            $credentials = [
                'email' => $input['email'],
                'password' => $input['password'],
                'user_type' => 'customer'
            ];
            try {
                if (!$token = JWTAuth::attempt($credentials)) {
                    return response()->json([
                        'message' => 'Authentication Failed.',
                        'errorcode' => 1
                    ], 200);
                }
            } catch (JWTException $e) {
                return response()->json([
                    'message' => 'Could not create token.',
                    'errorcode' => 1
                ], 1);
            }

            return response()->json([
                'message' => "Success",
                'data' => (object)[
                    'token' => $token,
                ],
                'errorcode' => 0
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'errorcode' => 1
            ], 1);
        }
    }

    // Register a user
    public function register(Request $request)
    {
        $rules = [
            'first_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->messages(), 'errorcode' => 1], 200);
        }
        $input = $request->all();
        $this->userService->storeUser($input);

        return response()->json([
            'message' => "Success",
            'errorcode' => 0
        ], 200);

    }
}
