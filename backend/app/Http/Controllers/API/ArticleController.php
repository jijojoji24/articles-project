<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Services\ArticleService;
use App\Services\UserService;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    protected $articleService;
    protected $userService;

    public function __construct(
        ArticleService $articleService,
        UserService $userService
    ){
        $this->articleService = $articleService;
        $this->userService = $userService;
    }
    public function index()
    {
        if($user = getJwtUser()){
            $articles = $this->articleService->getArticles($user);
            $sources = $this->articleService->getArticleSources($user);
        }else{
            return response()->json([
                'data' => [],
                'status' => 'error'
            ]);
        }

        return response()->json([
            'data' => [
                'articles' => $articles,
                'sources' => $sources
            ],'status' => 'OK'
        ]);
    }

    public function settings()
    {
        if($user = getJwtUser()){
            $keywords = $this->userService->getPreference($user);
            $sources = $this->userService->getSource($user);
        }else{
            return response()->json([
                'data' => [],
                'status' => 'error'
            ]);
        }

        return response()->json([
            'data' => [
                'preference' => $keywords,
                'sources' => $sources
            ],'status' => 'OK'
        ]);
    }

    public function saveSettings(Request $request){
        if($user = getJwtUser()){

            $input = $request->all();
            $input['user_id'] = $user->id;
            $this->userService->storeSettings($input);
            return response()->json([
                'message' => "Saved successully",
                "errorcode" => 0,
                'status' => 'OK'
            ]);
        }

    }
}
