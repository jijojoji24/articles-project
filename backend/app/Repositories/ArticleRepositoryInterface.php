<?php

namespace App\Repositories;

interface ArticleRepositoryInterface
{
    public function getArticles($user_id);
    public function getArticleSources($user_id);
}
