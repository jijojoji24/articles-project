<?php

namespace App\Repositories;

use App\Models\Article;
use App\Models\ArticleSource;
use App\Models\User;
use App\Models\UserPreference;
use App\Models\UserSource;
use Illuminate\Support\Facades\Hash;

class UserRepository implements UserRepositoryInterface
{
    public function __construct()
    {
    }
    public function getUsers()
    {
        return User::where('user_type','customer')->get();
    }

    public function getUserById($id)
    {
        return User::find($id);
    }

    public function storeUser($payload)
    {

        $arr = [
            'first_name' => $payload['first_name'],
            'last_name' => isset($payload['last_name'])?$payload['last_name']:'',
            'email' => $payload['email'],
            'password' => Hash::make($payload['password']),
            'status' => 'active',
            'user_type' => 'customer'
        ];

        $user = User::create($arr);

        $articles_sources = ArticleSource::orderBy('id','ASC')->get();

        foreach ($articles_sources as $key => $source) {
            UserSource::create([
                'source_id' => $source->id,
                'user_id' => $user->id,
                'status' => 'on'
            ]);
        }

        UserPreference::create([
            'keyword' => '',
            'user_id' => $user->id
        ]);

        return $user;
    }

    public function getUserSources($id)
    {
        return UserSource::where('user_id',$id)->get();
    }

    public function getPreference($id)
    {
        return UserPreference::where('user_id',$id)->first()->keyword;
    }

    public function getSource($id)
    {
        return ArticleSource::with(['user_source' => function($q) use($id){
            $q->where('user_id',$id);
        }])->get();
    }


    public function storeSettings($payload)
    {
        UserSource::where('user_id',$payload['user_id'])
            ->whereNotIn('id',$payload['source'])
            ->update([
                'status'=> 'off'
            ]);

        UserSource::where('user_id',$payload['user_id'])
            ->whereIn('id',$payload['source'])
            ->update([
                'status'=> 'on'
            ]);


        UserPreference::where('user_id',$payload['user_id'])->update([
            'keyword' => isset($payload['preference'])?$payload['preference']:''
        ]);
    }
}
