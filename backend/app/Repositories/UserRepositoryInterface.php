<?php

namespace App\Repositories;

interface UserRepositoryInterface
{
    public function getUsers();
    public function getUserById($id);
    public function storeUser($payload);
    public function getPreference($id);
    public function getSource($id);
    public function storeSettings($payload);

}
