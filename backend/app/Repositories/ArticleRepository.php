<?php

namespace App\Repositories;

use App\Models\Article;
use App\Models\ArticleSource;
use App\Models\User;
use App\Models\UserPreference;
use App\Models\UserSource;

class ArticleRepository implements ArticleRepositoryInterface
{
    public function __construct()
    {
        //
    }
    public function getArticles($user_id)
    {
        $sources = UserSource::where('user_id',$user_id)->where('status','on')->pluck('source_id')->toArray();
        $preference = UserPreference::where('user_id',$user_id)->first();

        if($preference && $preference->keyword != ''){
            $keywords = $preference->keyword;

            $results = Article::whereIn('source_id',$sources)
            ->orderByRaw("CASE WHEN title LIKE '%".$keywords."%' THEN 0 ELSE 1 END");
        }else{
            $results = Article::whereIn('source_id',$sources)
            ->orderBy('source_id', 'ASC');
        }


        if(isset($_GET['search']) && $_GET['search'] != ''){
            $search = htmlspecialchars($_GET['search']);

            $results = $results->where(function($q) use($search){
                $q->where('title','LIKE', "%{$search}%")
                    ->orWhere('description','LIKE', "%{$search}%");
            });
        }

        if(isset($_GET['source_id']) && $_GET['source_id'] != ''){
            $source_id = htmlspecialchars($_GET['source_id']);

            $results = $results->where('source_id',$source_id);
        }

        $results = $results->get();

        return $results;
    }
    public function getArticleSources($user_id)
    {
        $sources = ArticleSource::whereHas('user_source',function($q) use($user_id){
            $q->where('user_id',$user_id)->where('status','on');
        })->get();

        return $sources;
    }
}
