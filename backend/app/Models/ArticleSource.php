<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArticleSource extends Model
{
    use HasFactory;

    protected $fillable = [
        'source_title',
        'api_key'
    ];

    public function user_source(){
        return $this->hasOne(UserSource::class,'source_id');
    }
}
