import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import { Head } from '@inertiajs/react';
import DataTable from 'react-data-table-component';




export default function Dashboard({ auth,users }) {
    const columns = [
        {
            name: 'Name',
            selector: row => row.first_name+' '+(row.last_name?row.last_name:''),
        },
        {
            name: 'Email',
            selector: row => row.email,
        },
    ];

    const data = users;

    return (
        <AuthenticatedLayout
            user={auth.user}
            header={<h2 className="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">Users</h2>}
        >
            <Head title="Dashboard" />

            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <DataTable
                    columns={columns}
                    data={data}
                />
                </div>
            </div>
        </AuthenticatedLayout>
    );
}
