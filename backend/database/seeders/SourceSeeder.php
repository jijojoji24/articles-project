<?php

namespace Database\Seeders;

use App\Models\ArticleSource;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class SourceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        ArticleSource::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        if(!ArticleSource::first()){
            ArticleSource::insert([[
                'source_title' =>'NewsAPI.org',
                'api_key' => '9a97efd219d341749a2474bdff131220',
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'source_title' =>'New York Times',
                'api_key' => 'AaPgmxZWDOi9t7rZEM5BvGZcr6n0RNsJ',
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'source_title' =>'Newsdata.IO',
                'api_key' => 'pub_205166b75514f355301c8405497998f74d12a',
                'created_at' => date('Y-m-d H:i:s')
            ],
        ]);
        }
    }
}
