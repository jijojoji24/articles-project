<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        if(!User::where('user_type','admin')->first()){
            User::create([
                'first_name' =>'Article',
                'last_name' =>'Admin',
                'email' => 'admin@articles.com',
                'password' => Hash::make('12345678')
            ]);
        }
    }
}
