import './App.css';
import Dashboard from './components/Dashboard';
import Setting from './components/Setting';
import SignIn from './components/SignIn';
import SignUp from './components/Signup';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

function App() {
  return (
    <Router>
      <Routes>
        <Route exact path="/login" element={<SignIn />} />
        <Route exact path="/register" element={<SignUp />} />
        <Route exact path="/" element={<Dashboard />} />
        <Route exact path="/settings" element={<Setting />} />
      </Routes>
    </Router>
  );
}

export default App;
