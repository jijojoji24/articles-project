import * as React from 'react';

import { useState, useEffect } from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';

import { useNavigate } from 'react-router-dom';
import { Alert,AlertTitle, FormControl, FormGroup, IconButton } from '@mui/material';
import { useFormik } from 'formik';
import * as yup from 'yup';
import axios from 'axios';

import { Link as RouterLink } from "react-router-dom";
import { Logout } from '@mui/icons-material';

const theme = createTheme();

export default function Setting() {
  const navigate = useNavigate();
  const [sources, setSources] = useState([]);
  const [preference, setPreference] = useState('');
  const [userSources, setUserSources] = useState([]);

  const [severity, setSeverity] = useState('');
  const [message, setMessage] = useState('');
  const [alerttitle, setAlerttitle] = useState('');

  useEffect(() => {
    fetchSettings()
  }, []);

  const fetchSettings = () => {
    if(localStorage.getItem('token')){
      const authString = 'Bearer '+localStorage.getItem('token')

      axios.get('http://127.0.0.1:8000/api/settings',
        {
        headers: { Authorization: authString } })
        .then(response => {
        if (response.data.status == "OK") {
            setSources(response.data.data.sources)
            setPreference(response.data.data.preference)
            let arr = []
            response.data.data.sources.forEach(element => {
              if(element.user_source.status == 'on'){
                arr.push(element.user_source.id)
              }
            });
            setUserSources(arr);
              
        }else{
            navigate('/login')
        }
      })
      .catch(error => {
        navigate('/login')
      });
    }else{
      navigate('/login')
    }
  }

  const handleCheckboxChange = (event) => {
    const { value, checked } = event.target;
  
    let number = parseInt(value)
    if(checked){
      setUserSources((prevSource) => [...prevSource, number]);
    }else{
      const newArray = userSources.filter((item) => item !== number);

      setUserSources(newArray);
    }
  };

  const handlePreferece = (event) => {
    setPreference(event.target.value)
  }
  const handleSubmit = (event) => {
    event.preventDefault();
    const payload = {
        source: userSources,
        preference: preference
      };
      if(localStorage.getItem('token')){
        const authString = 'Bearer '+localStorage.getItem('token')

        axios.post('http://127.0.0.1:8000/api/save-settings',payload, 
          {headers: { Authorization: authString } } )
        .then(response => {
          console.log(response.data);
          if (response.data.errorcode == 0) {
            setSeverity('success') 
            setMessage(response.data.message)
            setAlerttitle('Success')
            setTimeout(() => {
              navigate('/');
            }, 1000);
          }else{
            setSeverity('error') 
            setMessage(response.data.message)
            setAlerttitle('Error')
          }
        })
        .catch(error => {
          console.error(error);
        });
      
      }
    
  };
  const logout = (event) => {
    localStorage.removeItem('token')
    navigate('login')
  };
  return (
    
    <>
    <CssBaseline />
    <Container maxWidth="lg" sx={{ marginTop: 5, marginBottom: 5 }}>
        <Grid container spacing={2}>
            <Grid item xs={10}>
            <h1>News Preferences</h1>
            </Grid>
            <Grid item xs={2}>

          
              <IconButton aria-label="Logout"
              onClick={logout}
              >
              <Logout />
              </IconButton>
            </Grid>
        </Grid>
        <Box component="form" sx={{ flexGrow: 1 }} noValidate onSubmit={handleSubmit}>
           {severity!='' && <Alert severity={severity}>
              <AlertTitle>{alerttitle}</AlertTitle>{message}</Alert>}
            <FormGroup>
                 { sources.map((source) => (
                    <FormControlLabel control={<Checkbox 
                      onChange={handleCheckboxChange} defaultChecked checked = {userSources.includes(source.user_source.id)} value = {source.user_source.id} />} label={source.source_title} />
                ))}
                <TextField
                    name="preference"
                    required
                    fullWidth
                    onChange={handlePreferece}
                    value={preference}
                    id="preference"
                    label="Keywords (Comma seperated)"
                />

            </FormGroup>
            
          <Button
                type="submit"
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
              >
              Save changes
            </Button>
        </Box>
    </Container>
  </>
  );
}