import React from 'react';

const Filter = ({ filterNews }) => {
  const handleFilterChange = (event) => {
    filterNews(event.target.value);
  };

  return (
    <div>
      <label htmlFor="filter">Filter by category:</label>
      <select id="filter" onChange={handleFilterChange}>
        <option value="all">All categories</option>
        <option value="technology">Technology</option>
        <option value="travel">Travel</option>
        <option value="food">Food</option>
      </select>
    </div>
  );
};

export default Filter;