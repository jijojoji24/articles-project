import { makeStyles } from '@mui/styles';
import { Typography, Divider, IconButton, Link } from '@mui/material';
import { ArrowBack } from '@mui/icons-material';

const useStyles = makeStyles((theme) => ({
  link : {
    color: 'unset',
    textDecoration: 'none'
  },
  root: {
    maxWidth: 800,
    margin: '0 auto',
    padding: 2,
  },
  header: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: 2,
  },
  backButton: {
    marginRight: 2,
  },
  title: {
    flexGrow: 1,
    fontWeight:800
  },
  content: {
    marginTop: 2,
  },
  image: {
    maxWidth: '100%',
    marginBottom: 2,
  },
}));

function SingleBlog(props) {
  const classes = useStyles();

  return (
    <Link href={props.url} target='_blank' className={classes.link}>
      <div className={classes.root}>
        <div className={classes.header}>
          {/* <IconButton className={classes.backButton} onClick={() => props.history.goBack()}>
            <ArrowBack />
          </IconButton> */}
          <Typography variant="h6" className={classes.title}>
            {props.title}
          </Typography>
        </div>
        <Divider />
        <div className={classes.content}>
          {props.image && (
            <img src={props.image} alt={props.title} className={classes.image} />
          )}
          <Typography variant="body1">{props.description}</Typography>
        </div>
      </div>
    </Link>
    
  );
}

export default SingleBlog;