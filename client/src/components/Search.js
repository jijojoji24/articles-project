import React from 'react';

const Search = ({ searchTerm, searchBlogs }) => {
  return (
    <div>
      <label htmlFor="search">Search by title:</label>
      <input
        type="text"
        id="search"
        value={searchTerm}
        onChange={searchBlogs}
      />
    </div>
  );
};

export default Search;



