import React, { useState, useEffect } from 'react';
import News from './SingleBlog';
import Filter from './Filter';
import Search from './Search';
import { styled } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import SingleBlog from './SingleBlog';
import axios from 'axios';
import { Navigate, useNavigate } from 'react-router-dom';
import { Button, FormControl, FormGroup, IconButton, InputLabel, MenuItem, Select, TextField } from '@mui/material';
import { Link as RouterLink } from "react-router-dom";
import { Logout, Settings } from '@mui/icons-material';
const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

const Dashboard = () => {
  const [blogs, setBlogs] = useState([]);
  const [sources, setSources] = useState([]);
  const [source, setSource] = useState('');
  const [search, setSearch] = useState('');

  const navigate = useNavigate();
  // Fetch news from an API or database
  useEffect(() => {
    fetchArticles()
  }, [source,search]);

  const fetchArticles = () => {
    if(localStorage.getItem('token')){
      const authString = 'Bearer '+localStorage.getItem('token')

      axios.get('http://127.0.0.1:8000/api/articles',{ params: {
          source_id: source,
          search: search
        },
        headers: { Authorization: authString } })
        .then(response => {
        if (response.data.status == "OK") {
          setBlogs(response.data.data.articles)
          setSources(response.data.data.sources)
        }else{
           navigate('/login')
        }
      })
      .catch(error => {
        navigate('/login')
      });
    }else{
      navigate('/login')
    }
  }

  const handleChange = (event) => {
    setSource(event.target.value);
  };

  const handleSearch = (event) => {
    setSearch(event.target.value);
  };

  const logout = (event) => {
    localStorage.removeItem('token')
    navigate('login')
  };

  return (
    <>
      <CssBaseline />
      <Container maxWidth="lg" sx={{ marginTop: 5, marginBottom: 5 }}>
        <Grid container spacing={2}>
          <Grid item xs={10}>
            <h1>News and Articles</h1>
          </Grid>
          <Grid item xs={2}>

            <RouterLink to="/settings" color="inherit">
              <IconButton aria-label="Settings">
                <Settings />
              </IconButton>
            </RouterLink>
            <IconButton aria-label="Logout" onClick={logout}>
              <Logout />
            </IconButton>
          </Grid>
          <Grid item xs={6}>
            <FormGroup>
              <FormControl>
              <TextField id="outlined-basic" label="Search" variant="outlined"
                value={search}
                onChange={handleSearch}
              />
              </FormControl>
              </FormGroup>
          </Grid>
          <Grid item xs={6}>
            <FormGroup>
            <FormControl >
            <InputLabel id="demo-simple-select-label">Select Source</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={source}
              onChange={handleChange}
              label="Select an option"
            >
              <MenuItem value="" key="0">All Sources</MenuItem>
              { sources.map((source,index) => (
                <MenuItem key={index} value={source.id}>{source.source_title}</MenuItem>
              ))}
            </Select>
            </FormControl>

            </FormGroup>
          </Grid>
      </Grid>
      <Box sx={{ flexGrow: 1 }}>
        <Grid container spacing={2}>
          { blogs.map((blog) => (
            <Grid item xs={10}>
              <SingleBlog {...blog} />
            </Grid>
          ))}
        </Grid>
      </Box>
      </Container>
    </>
  );
};

export default Dashboard;